﻿<%@ Page Title="Contact" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="PatientInfo.aspx.cs" Inherits="lab02_ehealth.PatientInfo" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2>Medical details of <asp:Label ID="lblName" runat="server" CssClass="fn_9" Width="250px" MaxLength="600"></asp:Label> </h2>
    <h4>Gender:</h4><asp:Label ID="lblGender" runat="server"></asp:Label> 
    <h4>Diagnosis Description:</h4><asp:Label ID="lblDiag" runat="server" ></asp:Label>
    <h4>Diagnosis Description Detailed:</h4><asp:Label ID="lblDiagDet" runat="server"></asp:Label>
    <h4>Treatment:</h4><asp:Label ID="lblTreatment" runat="server"></asp:Label>
   

    <address>
        <strong>Contact:</strong><asp:Label ID="lblEmail" runat="server" CssClass="fn_9" Width="250px" MaxLength="600"></asp:Label> <br />
    </address>
</asp:Content>
