﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Patients.aspx.cs" Inherits="lab02_ehealth.Patients" %>


<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <br />
    <asp:TextBox ID="searchBox" runat="server"></asp:TextBox>
    <asp:Button ID="searchButton" runat="server" Text="search" OnClick="searchButton_Click" />
    <asp:Button ID="reset" runat="server" Text="reset" OnClick="resetSearchButton_Click" />
    <br />
    <br />
    <asp:GridView ID="gvPatients" PageSize="5" AllowPaging="True" OnPageIndexChanging="gvPatients_PageIndexChanging" OnRowCommand="OnRowCommand" runat="server" AutoGenerateColumns="False" Visible="true">
    <Columns>
        <asp:BoundField DataField="id" HeaderText="Id" Visible="false" ItemStyle-Width="150" />
        <asp:BoundField DataField="first_name" HeaderText="First Name" ItemStyle-Width="150" />
        <asp:BoundField DataField="last_name" HeaderText="Last Name" ItemStyle-Width="150" />
        <asp:BoundField DataField="email" HeaderText="Email" Visible="false" ItemStyle-Width="150" />
        <asp:BoundField DataField="gender" HeaderText="Gender" Visible="false" ItemStyle-Width="150" />
        <asp:BoundField DataField="diagnosis_description" HeaderText="Diagnosis Description" Visible="false" ItemStyle-Width="150" />
        <asp:BoundField DataField="diagnosis_description_detailed" HeaderText="Diagnosis Description Detailed" Visible="false" ItemStyle-Width="150" />
        <asp:BoundField DataField="treatment_given" HeaderText="Given Treatment" Visible="false" ItemStyle-Width="150" />
        <asp:TemplateField >
                                 
                                    <ItemTemplate>
                                        <asp:ImageButton ID="lnkEdit" runat="server" ImageUrl="images/plus.png" 
                                            onclick="btnSave_Click"
                                                        CommandName="Editare" 
                                            CommandArgument = '<%# Eval("id") + "#" + Eval("first_name") + "#" + Eval("last_name") + "#" + Eval("email") + "#" 
                                                                + Eval("gender") + "#" + Eval("diagnosis_description") + "#" 
                                                                + Eval("diagnosis_description_detailed") + "#" + Eval("treatment_given")%>' 
                                            ToolTip="Modificare"  />                                      
                                    </ItemTemplate>
                                </asp:TemplateField>
    </Columns>
</asp:GridView>

</asp:Content>
