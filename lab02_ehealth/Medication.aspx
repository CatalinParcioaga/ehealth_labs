﻿<%@ Page Title="Medication" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Medication.aspx.cs" Inherits="lab02_ehealth.Medication" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <br />
    <asp:GridView ID="gvPatients" PageSize="5" AllowPaging="True" OnPageIndexChanging="gvPatients_PageIndexChanging" OnRowCommand="OnRowCommand" runat="server" AutoGenerateColumns="False" Visible="true">
    <Columns>
        <asp:BoundField DataField="id" HeaderText="Id" ItemStyle-Width="150" />
        <asp:BoundField DataField="diagnosis_description" HeaderText="Diagnosis Description" ItemStyle-Width="150" />
        <asp:BoundField DataField="diagnosis_description_detailed" HeaderText="Diagnosis Description Detailed" ItemStyle-Width="150" />
        <asp:BoundField DataField="treatment_given" HeaderText="Given Treatment" ItemStyle-Width="150" />
    </Columns>
</asp:GridView>

</asp:Content>
