﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(lab02_ehealth.Startup))]
namespace lab02_ehealth
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
