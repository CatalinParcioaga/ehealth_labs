﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace lab02_ehealth.Models
{
    public class Medication
    {
        public string id { get; set; }
        public string diagnosis_description { get; set; }
        public string diagnosis_description_detailed { get; set; }
        public string administered_drug_treatment { get; set; }
    }
}