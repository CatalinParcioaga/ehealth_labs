﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace lab02_ehealth
{
    public partial class PatientInfo : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string firstName = (string)Request["firstname"];
            string lastName = (string)Request["lastname"];
            string email = (string)Request["email"];
            string gender = (string)Request["gender"];
            string diagnosisDescription = (string)Request["diagdescription"];
            string diagnosisDetails = (string)Request["diagdet"];
            string treatment = (string)Request["treatment"];

            lblEmail.Text = email;
            lblName.Text = firstName + " " + lastName;
            lblGender.Text = gender;
            lblDiag.Text = diagnosisDescription;
            lblDiagDet.Text = diagnosisDetails;
            lblTreatment.Text = treatment;
        }
    }
}