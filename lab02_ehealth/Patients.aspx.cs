﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json;
using System.Net;
using lab02_ehealth.Models;
using System.Data;
using Newtonsoft.Json.Linq;
using System.IO;

namespace lab02_ehealth
{
    public partial class Patients : Page
    {
        public List<Patient> lst;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                BindGridView();
            }
        }

        private void BindGridView()
        {
            var webclient = new WebClient();
            var json = webclient.DownloadString(@"https://alexgr.ro/ehealth/patients.json");
            JArray jArray = JArray.Parse(json);

            DataTable dt = new DataTable();
            dt.Columns.AddRange(new DataColumn[8] { new DataColumn("id", typeof(string)),
                                                    new DataColumn("first_name", typeof(string)),
                                                    new DataColumn("last_name", typeof(string)),
                                                    new DataColumn("email", typeof(string)),
                                                    new DataColumn("gender", typeof(string)),
                                                    new DataColumn("diagnosis_description", typeof(string)),
                                                    new DataColumn("diagnosis_description_detailed", typeof(string)),
                                                    new DataColumn("treatment_given", typeof(string))});
     
            lst = jArray.ToObject<List<Patient>>();
            
            foreach (Patient patient in lst)
            {
                dt.Rows.Add(patient.id, patient.first_name, patient.last_name, patient.email, patient.gender, patient.diagnosis_description,
                    patient.diagnosis_description_detailed, patient.administered_drug_treatment);
            }
            ViewState["myViewState"] = dt;

            gvPatients.DataSource = dt;
            gvPatients.DataBind();
        }

        protected void OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            var value = e.CommandArgument.ToString().Split('#');
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            var value = ((ImageButton)sender).CommandArgument.ToString().Split('#');
            string v_url = "PatientInfo.aspx?id=" + value[0] + "&firstname=" + value[1] + "&lastname=" + value[2] + "&email=" + value[3]
                + "&gender=" + value[4] + "&diagdescription=" + value[5] + "&diagdet=" + value[6] + "&treatment=" + value[7];
            Response.Redirect(v_url);
           
        }

        protected void gvPatients_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvPatients.PageIndex = e.NewPageIndex;
            BindGridView();
        }

        protected void searchButton_Click(object sender, EventArgs e)
        {
            string searchTerm = searchBox.Text.ToLower();

            //check if the search input is at least 3 chars
            if (searchTerm.Length >= 3)
            {
                //always check if the viewstate exists before using it
                if (ViewState["myViewState"] == null)
                    return;

                //cast the viewstate as a datatable
                DataTable dt = ViewState["myViewState"] as DataTable;

                //make a clone of the datatable
                DataTable dtNew = dt.Clone();

                //search the datatable for the correct fields
                foreach (DataRow row in dt.Rows)
                {
                    //add your own columns to be searched here
                    if (row["first_name"].ToString().ToLower().Contains(searchTerm) || row["last_name"].ToString().ToLower().Contains(searchTerm))
                    {
                        //when found copy the row to the cloned table
                        dtNew.Rows.Add(row.ItemArray);
                    }
                }

                //rebind the grid
                gvPatients.DataSource = dtNew;
                gvPatients.DataBind();
            }
        }


        protected void resetSearchButton_Click(object sender, EventArgs e)
        {
            //always check if the viewstate exists before using it
            if (ViewState["myViewState"] == null)
                return;

            //cast the viewstate as a datatable
            DataTable dt = ViewState["myViewState"] as DataTable;

            //rebind the grid
            gvPatients.DataSource = dt;
            gvPatients.DataBind();
        }
    }
}