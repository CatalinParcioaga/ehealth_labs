﻿using lab02_ehealth.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace lab02_ehealth
{
    public partial class Medication : Page
    {
        public List<Patient> lst;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                BindGridView();
            }
        }

        private void BindGridView()
        {
            var webclient = new WebClient();
            var json = webclient.DownloadString(@"https://alexgr.ro/ehealth/patients.json");
            JArray jArray = JArray.Parse(json);

            DataTable dt = new DataTable();

            dt.Columns.AddRange(new DataColumn[4] { new DataColumn("id", typeof(string)),
                                                    new DataColumn("diagnosis_description", typeof(string)),
                                                    new DataColumn("diagnosis_description_detailed", typeof(string)),
                                                    new DataColumn("treatment_given", typeof(string))});

            lst = jArray.ToObject<List<Patient>>();

            foreach (Patient patient in lst)
            {
                dt.Rows.Add(patient.id, patient.diagnosis_description,
                    patient.diagnosis_description_detailed, patient.administered_drug_treatment);
            }
            ViewState["myViewState"] = dt;

            gvPatients.DataSource = dt;
            gvPatients.DataBind();
        }

        protected void OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            var value = e.CommandArgument.ToString().Split('#');
        }


        protected void gvPatients_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvPatients.PageIndex = e.NewPageIndex;
            BindGridView();
        }

    }
}